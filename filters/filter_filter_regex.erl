-module(filter_filter_regex).
-author("steffen").

%% API
-export([
  filter_regex/4
]).


filter_regex(_, undefined, _, _Context) -> [];
filter_regex(undefined, _, _, _Context) -> [];
filter_regex(In, Prop, Expression, Context) ->
    lists:filter(fun(Elt) ->
      case re:run(find_value(Prop, Elt, Context), Expression) of
        match ->
          true;
        nomatch ->
          false;
        {match, _} ->
          true
      end
  end,
    erlydtl_runtime:to_list(In, Context)).


find_value(Prop, Elt, Context) ->
  erlydtl_runtime:find_value(Prop, Elt, Context).

