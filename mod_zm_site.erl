-module(mod_zm_site).
-author("Steffen Hanikel <steffen.hanikel@gmail.com>").

-mod_title("Zeitmacht Site").
-mod_description("Zeitmacht site basics.").
-mod_prio(550).
-mod_depends([base]).
-mod_provides([base_site]).
